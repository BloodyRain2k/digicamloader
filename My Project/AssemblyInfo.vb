﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Digicam Loader")> 
<Assembly: AssemblyDescription("For support or feedback contact me on chaoscloud@web.de")> 
<Assembly: AssemblyCompany("")> 
<Assembly: AssemblyProduct("Digicam Loader")> 
<Assembly: AssemblyCopyright("Copyright ©  2007 Bernhard -BloodyRain- Kroetsch")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("8c098957-fda1-462a-b7a8-3f169716c4f0")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.0")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 
