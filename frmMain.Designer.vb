<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
	Inherits System.Windows.Forms.Form

	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> _
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing AndAlso components IsNot Nothing Then
			components.Dispose()
		End If
		MyBase.Dispose(disposing)
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> _
	Private Sub InitializeComponent()
		Me.components = New System.ComponentModel.Container
		Me.lstCamMasks = New System.Windows.Forms.ListBox
		Me.mnulstContext = New System.Windows.Forms.ContextMenuStrip(Me.components)
		Me.AddToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
		Me.RemoveToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
		Me.barProgress2 = New System.Windows.Forms.ProgressBar
		Me.cmdScan = New System.Windows.Forms.Button
		Me.txtSavePath = New System.Windows.Forms.TextBox
		Me.tmrProgress = New System.Windows.Forms.Timer(Me.components)
		Me.lstDrives = New System.Windows.Forms.ListBox
		Me.barProgressTotal = New System.Windows.Forms.ProgressBar
		Me.lblTimeLeft = New System.Windows.Forms.Label
		Me.MenuStrip1 = New System.Windows.Forms.MenuStrip
		Me.mnuDonate = New System.Windows.Forms.ToolStripMenuItem
		Me.DummyToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
		Me.tmrDummy = New System.Windows.Forms.Timer(Me.components)
		Me.chkAutoScan = New System.Windows.Forms.CheckBox
		Me.tmrAS = New System.Windows.Forms.Timer(Me.components)
		Me.mnulstContext.SuspendLayout()
		Me.MenuStrip1.SuspendLayout()
		Me.SuspendLayout()
		'
		'lstCamMasks
		'
		Me.lstCamMasks.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
					Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.lstCamMasks.ContextMenuStrip = Me.mnulstContext
		Me.lstCamMasks.FormattingEnabled = True
		Me.lstCamMasks.IntegralHeight = False
		Me.lstCamMasks.Location = New System.Drawing.Point(4, 54)
		Me.lstCamMasks.Name = "lstCamMasks"
		Me.lstCamMasks.Size = New System.Drawing.Size(168, 138)
		Me.lstCamMasks.Sorted = True
		Me.lstCamMasks.TabIndex = 0
		'
		'mnulstContext
		'
		Me.mnulstContext.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AddToolStripMenuItem, Me.RemoveToolStripMenuItem})
		Me.mnulstContext.Name = "mnulstContext"
		Me.mnulstContext.Size = New System.Drawing.Size(115, 48)
		'
		'AddToolStripMenuItem
		'
		Me.AddToolStripMenuItem.Name = "AddToolStripMenuItem"
		Me.AddToolStripMenuItem.Size = New System.Drawing.Size(114, 22)
		Me.AddToolStripMenuItem.Text = "&Add"
		'
		'RemoveToolStripMenuItem
		'
		Me.RemoveToolStripMenuItem.Name = "RemoveToolStripMenuItem"
		Me.RemoveToolStripMenuItem.Size = New System.Drawing.Size(114, 22)
		Me.RemoveToolStripMenuItem.Text = "&Remove"
		'
		'barProgress2
		'
		Me.barProgress2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
					Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.barProgress2.Location = New System.Drawing.Point(4, 198)
		Me.barProgress2.Maximum = 1000
		Me.barProgress2.Name = "barProgress2"
		Me.barProgress2.Size = New System.Drawing.Size(168, 10)
		Me.barProgress2.Style = System.Windows.Forms.ProgressBarStyle.Continuous
		Me.barProgress2.TabIndex = 1
		'
		'cmdScan
		'
		Me.cmdScan.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.cmdScan.Location = New System.Drawing.Point(250, 198)
		Me.cmdScan.Name = "cmdScan"
		Me.cmdScan.Size = New System.Drawing.Size(63, 20)
		Me.cmdScan.TabIndex = 2
		Me.cmdScan.Text = "&Scan"
		Me.cmdScan.UseVisualStyleBackColor = True
		'
		'txtSavePath
		'
		Me.txtSavePath.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
					Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.txtSavePath.Location = New System.Drawing.Point(4, 28)
		Me.txtSavePath.Name = "txtSavePath"
		Me.txtSavePath.Size = New System.Drawing.Size(309, 20)
		Me.txtSavePath.TabIndex = 3
		Me.txtSavePath.Text = "C:\DigiCam Pics"
		'
		'tmrProgress
		'
		Me.tmrProgress.Interval = 50
		'
		'lstDrives
		'
		Me.lstDrives.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
					Or System.Windows.Forms.AnchorStyles.Left) _
					Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.lstDrives.ContextMenuStrip = Me.mnulstContext
		Me.lstDrives.FormattingEnabled = True
		Me.lstDrives.IntegralHeight = False
		Me.lstDrives.Location = New System.Drawing.Point(178, 54)
		Me.lstDrives.Name = "lstDrives"
		Me.lstDrives.Size = New System.Drawing.Size(135, 115)
		Me.lstDrives.Sorted = True
		Me.lstDrives.TabIndex = 5
		'
		'barProgressTotal
		'
		Me.barProgressTotal.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
					Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.barProgressTotal.Location = New System.Drawing.Point(4, 208)
		Me.barProgressTotal.Maximum = 1000
		Me.barProgressTotal.Name = "barProgressTotal"
		Me.barProgressTotal.Size = New System.Drawing.Size(168, 10)
		Me.barProgressTotal.Style = System.Windows.Forms.ProgressBarStyle.Continuous
		Me.barProgressTotal.TabIndex = 6
		'
		'lblTimeLeft
		'
		Me.lblTimeLeft.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.lblTimeLeft.AutoSize = True
		Me.lblTimeLeft.BackColor = System.Drawing.SystemColors.Control
		Me.lblTimeLeft.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.lblTimeLeft.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lblTimeLeft.Location = New System.Drawing.Point(178, 198)
		Me.lblTimeLeft.Name = "lblTimeLeft"
		Me.lblTimeLeft.Size = New System.Drawing.Size(66, 20)
		Me.lblTimeLeft.TabIndex = 8
		Me.lblTimeLeft.Text = "00:00:00"
		'
		'MenuStrip1
		'
		Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuDonate, Me.DummyToolStripMenuItem})
		Me.MenuStrip1.Location = New System.Drawing.Point(1, 1)
		Me.MenuStrip1.Name = "MenuStrip1"
		Me.MenuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
		Me.MenuStrip1.Size = New System.Drawing.Size(315, 24)
		Me.MenuStrip1.TabIndex = 9
		Me.MenuStrip1.Text = "MenuStrip1"
		'
		'mnuDonate
		'
		Me.mnuDonate.Name = "mnuDonate"
		Me.mnuDonate.Size = New System.Drawing.Size(54, 20)
		Me.mnuDonate.Text = "&Donate"
		'
		'DummyToolStripMenuItem
		'
		Me.DummyToolStripMenuItem.Name = "DummyToolStripMenuItem"
		Me.DummyToolStripMenuItem.Size = New System.Drawing.Size(54, 20)
		Me.DummyToolStripMenuItem.Text = "Dummy"
		Me.DummyToolStripMenuItem.Visible = False
		'
		'tmrDummy
		'
		'
		'chkAutoScan
		'
		Me.chkAutoScan.AutoSize = True
		Me.chkAutoScan.Location = New System.Drawing.Point(178, 175)
		Me.chkAutoScan.Name = "chkAutoScan"
		Me.chkAutoScan.Size = New System.Drawing.Size(76, 17)
		Me.chkAutoScan.TabIndex = 10
		Me.chkAutoScan.Text = "Auto Scan"
		Me.chkAutoScan.UseVisualStyleBackColor = True
		'
		'tmrAS
		'
		Me.tmrAS.Interval = 5000
		'
		'frmMain
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(317, 222)
		Me.Controls.Add(Me.chkAutoScan)
		Me.Controls.Add(Me.MenuStrip1)
		Me.Controls.Add(Me.txtSavePath)
		Me.Controls.Add(Me.lstDrives)
		Me.Controls.Add(Me.lstCamMasks)
		Me.Controls.Add(Me.lblTimeLeft)
		Me.Controls.Add(Me.cmdScan)
		Me.Controls.Add(Me.barProgressTotal)
		Me.Controls.Add(Me.barProgress2)
		Me.DoubleBuffered = True
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.MainMenuStrip = Me.MenuStrip1
		Me.Name = "frmMain"
		Me.Padding = New System.Windows.Forms.Padding(1)
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
		Me.Text = "Digicam Loader"
		Me.mnulstContext.ResumeLayout(False)
		Me.MenuStrip1.ResumeLayout(False)
		Me.MenuStrip1.PerformLayout()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub
	Friend WithEvents lstCamMasks As System.Windows.Forms.ListBox
	Friend WithEvents mnulstContext As System.Windows.Forms.ContextMenuStrip
	Friend WithEvents AddToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
	Friend WithEvents RemoveToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
	Friend WithEvents barProgress2 As System.Windows.Forms.ProgressBar
	Friend WithEvents cmdScan As System.Windows.Forms.Button
	Friend WithEvents txtSavePath As System.Windows.Forms.TextBox
	Friend WithEvents tmrProgress As System.Windows.Forms.Timer
	Friend WithEvents lstDrives As System.Windows.Forms.ListBox
	Friend WithEvents barProgressTotal As System.Windows.Forms.ProgressBar
	Friend WithEvents lblTimeLeft As System.Windows.Forms.Label
	Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
	Friend WithEvents mnuDonate As System.Windows.Forms.ToolStripMenuItem
	Friend WithEvents DummyToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
	Friend WithEvents tmrDummy As System.Windows.Forms.Timer
	Friend WithEvents chkAutoScan As System.Windows.Forms.CheckBox
	Friend WithEvents tmrAS As System.Windows.Forms.Timer

End Class
