Public Class frmMain
	Private mProgress As Single, mProgress2 As Single, UseFile As Byte, StpClk As New Stopwatch, mLastProgress As Single
	Private CheckHDD As Boolean = False, CheckCD As Boolean = False

	Private Sub AddToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddToolStripMenuItem.Click
		Dim a As String
		a = InputBox("New DigiCamPath-Mask")
		If a <> "" Then lstCamMasks.Items.Add(a) : SaveMasks()
	End Sub

	Private Sub RemoveToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RemoveToolStripMenuItem.Click
		If lstCamMasks.SelectedItem <> "" Then lstCamMasks.Items.Remove(lstCamMasks.SelectedItem) : SaveMasks()
	End Sub

	Private Sub SaveMasks()
		Dim i As Object, cCol As New Collection
		For Each i In lstCamMasks.Items
			cCol.Add(i)
		Next
		If FileIO.FileSystem.FileExists(AppPathExe() & ".vbcf") Then Kill(AppPathExe() & ".vbcf")
		SaveCollectionFile(cCol, AppPathExe)
	End Sub

	Private Sub frmMain_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
		WriteINI(AppPathExe(), "Settings", "SavePath", txtSavePath.Text)
		WriteINI(AppPathExe(), "Settings", "AutoScan", chkAutoScan.Checked)
		'UseFile = FreeFile()
		'FileOpen(UseFile, AppPathExe() & ".cfg", OpenMode.Output)
		'Print(UseFile, txtSavePath.Text)
		'FileClose(UseFile)
		End
	End Sub

	Private Sub frmMain_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		Dim cCol As New Collection, i As Object
		'If FileIO.FileSystem.FileExists(AppPathExe() & ".cfg") = True Then
		'UseFile = FreeFile()
		'FileOpen(UseFile, AppPathExe() & ".cfg", OpenMode.Input)
		'Input(UseFile, txtSavePath.Text)
		'FileClose(UseFile)
		'End If
		'txtSavePath.Text = My.Settings.SavePath
		txtSavePath.Text = ReadINI(AppPathExe(), "Settings", "SavePath", "C:\Digicam Pics")
		chkAutoScan.Checked = ReadINI(AppPathExe, "Settings", "AutoScan", False)
		LoadCollectionFile(cCol, AppPathExe())
		For Each i In cCol
			lstCamMasks.Items.Add(i)
		Next
		Me.cmdScan_Click(Me, System.EventArgs.Empty)
		'Me.Text += " - v" & AppVersion()
	End Sub

	Private Sub cmdScan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdScan.Click
		Dim i As Integer, lstMatches As New Collections.Generic.List(Of String)
		'Disable(cmdScan, lstCamMasks, txtSavePath)
		Me.Cursor = Cursors.WaitCursor
		lstDrives.Items.Clear()
		For i = 0 To FileIO.FileSystem.Drives.Count - 1 : DoEvents()
			If FileIO.FileSystem.Drives(i).IsReady = True Then
				Dim DriveText As String = FileIO.FileSystem.Drives(i).Name & " [" & FileIO.FileSystem.Drives(i).VolumeLabel & "]"
				If FileIO.FileSystem.Drives(i).DriveType = IO.DriveType.Fixed And CheckHDD = False Then GoTo SkipDrive
				If FileIO.FileSystem.Drives(i).DriveType = IO.DriveType.CDRom And CheckCD = False Then GoTo SkipDrive
				Dim Dirs As Collections.ObjectModel.ReadOnlyCollection(Of String)
				Dirs = FileIO.FileSystem.GetDirectories(FileIO.FileSystem.Drives(i).RootDirectory.FullName, FileIO.SearchOption.SearchTopLevelOnly)
				'ScanFoldersIntoCollection(Dirs, FileIO.FileSystem.Drives(i).RootDirectory.FullName, LongestMask() - 1)
				For Each Dir As String In Dirs
					If DirMatches(Dir) And Not lstDrives.Items.Contains(DriveText) Then lstDrives.Items.Add(DriveText) : Exit For
				Next
				'If IO.Directory.Exists(tmpGetPath) Then
				'lstDrives.Items.Add(FileIO.FileSystem.Drives(i).Name & " [" & FileIO.FileSystem.Drives(i).VolumeLabel & "]")
				'Exit For
				'End If
SkipDrive:
			End If
		Next
		Me.Cursor = Cursors.Default
		'Enable(cmdScan, lstCamMasks, txtSavePath)
	End Sub

	Private Function DirMatches(ByVal Dir As String) As Boolean
		Dim i As Integer, Mask As String, tmpMask As String = ""
		For i = lstCamMasks.Items.Count - 1 To 0 Step -1
			Mask = Dir.Substring(0, 3) & lstCamMasks.Items(i)
			If Dir.ToLower Like Mask.ToLower Then
				Return True
			Else
				Dim Splits() As String = Mask.Split("\"), a As Integer, x As Integer = Splits.Length
				For a = 1 To x - 1
					If a = 1 Then
						tmpMask = Dir.Substring(0, 3) & Splits(a)
					Else
						tmpMask += "\" + Splits(a)
					End If
					If Dir.ToLower Like tmpMask.ToLower Then
						If a = x Then
							Return True
						Else
							For Each tmpDir As String In FIOFS.GetDirectories(tmpMask, FileIO.SearchOption.SearchTopLevelOnly)
								Return DirMatches(tmpDir)
							Next
						End If
					End If
				Next
			End If
		Next
		Return False
	End Function

	Private Sub tmrProgress_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrProgress.Tick
		Dim ATimeUsed As Single, TimeLeft As Single, CurrElapsed As Single
		barProgress2.Value = mProgress2 * 10
		barProgressTotal.Value = mProgress * 10
		CurrElapsed = StpClk.ElapsedMilliseconds / 1000
		ATimeUsed = CurrElapsed / (mProgress * 10)
		If mLastProgress <> mProgress Then
			TimeLeft = (barProgress2.Maximum - (mProgress * 10)) * ATimeUsed
			'TimeLeft -= CurrElapsed
			If TimeLeft = Single.PositiveInfinity Then TimeLeft = 0
			If TimeLeft = Single.NegativeInfinity Then TimeLeft = 0
			lblTimeLeft.Text = MakeTimeString(, , TimeLeft)
		End If
		mLastProgress = mProgress
	End Sub

	Private Function LongestMask() As Int16
		Dim x, y As Int16
		For Each s As String In lstCamMasks.Items
			x = s.Split("\").Length
			If y < x Then y = x
		Next
		Return y
	End Function
	
	Private Function Month(ByVal m As Integer) As String
		Select Case m
			Case 1
				Return "01-January"
			Case 2
				Return "02-Febuary"
			Case 3
				Return "03-March"
			Case 4
				Return "04-April"
			Case 5
				Return "05-May"
			Case 6
				Return "06-June"
			Case 7
				Return "07-July"
			Case 8
				Return "08-August"
			Case 9
				Return "09-September"
			Case 10
				Return "10-October"
			Case 11
				Return "11-November"
			Case 12
				Return "12-December"
			Case Else
				Return "---"
		End Select
	End Function

	Private Sub lstDrives_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstDrives.DoubleClick
		Dim x As Object, tmpPutPath As String, tmpGetPath As String, fileDate As Date
		If Not FIOFS.DirectoryExists(txtSavePath.Text.Substring(0, 2)) Then MsgBox("Targetdrive does not exists. Please choose another drive.", MsgBoxStyle.Critical) : Exit Sub
		Disable(tmrAS)
		Me.Cursor = Cursors.WaitCursor
		WriteINI(AppPathExe(), "Settings", "SavePath", txtSavePath.Text)
		Dim Dirs As Collections.ObjectModel.ReadOnlyCollection(Of String), Files As New Collection
		Dirs = FileIO.FileSystem.GetDirectories(StrSplitter(lstDrives.SelectedItem, " "), FileIO.SearchOption.SearchAllSubDirectories)
		For Each Dir As String In Dirs
			For x = lstCamMasks.Items.Count To 1 Step -1
				tmpGetPath = StrSplitter(lstDrives.SelectedItem, " ") & lstCamMasks.Items(x - 1)
				If Dir.ToLower Like tmpGetPath.ToLower Then
					For Each f As String In FIOFS.GetFiles(Dir, FileIO.SearchOption.SearchAllSubDirectories)
						Files.Add(Dir & "|" & f)
					Next
					'tmpPutPath = txtSavePath.Text
					'If Strings.Right(tmpPutPath, 1) <> "\" Then tmpPutPath = tmpPutPath & "\"
					'tmpPutPath = tmpPutPath & Date.Now.Year & "\" & StrSplitter(String.Format("{0:M}", Date.Now), " ", 2) & "\" & Date.Now.Day & "\" & Strings.Left(Date.Now.TimeOfDay.ToString.Replace(":", "-"), 8) & "\"
					'StpClk.Reset()
					'StpClk.Start()
					'tmrProgress.Enabled = True
					'MoveFolder(tmpGetPath, tmpPutPath, mProgress, mProgress2)
					'StpClk.Stop()
					'tmrProgress.Enabled = False
				End If
			Next
		Next

		Dim TotalSize As Long = 0, SizeDone As Long = 0
		For Each F As String In Files
			TotalSize += FIOFS.GetFileInfo(F.Split("|")(1)).Length
		Next

		tmrProgress.Enabled = True
		StpClk.Reset()
		StpClk.Start()
		'tmpPutPath = tmpPutPath & Date.Now.Year & "\" & StrSplitter(String.Format("{0:M}", Date.Now), " ", 2) & "\" & Date.Now.Day & "\" & Strings.Left(Date.Now.TimeOfDay.ToString.Replace(":", "-"), 8) & "\"
		For Each F As String In Files
			Dim SourceDir As String = F.Split("|")(0)
			Dim SourceFile As String = F.Split("|")(1)
			Dim TargetFile As String = SourceFile.Substring(SourceDir.Length + 1)
			
			fileDate = FIOFS.GetFileInfo(SourceFile).LastWriteTime
			tmpPutPath = txtSavePath.Text
			If Strings.Right(tmpPutPath, 1) <> "\" Then tmpPutPath = tmpPutPath & "\"
			tmpPutPath = tmpPutPath & fileDate.Year & "\" & Month(fileDate.Month) & "\" & String.Format("{0:D2}", fileDate.Day) & "\"
		ren:
			If FIOFS.FileExists(tmpPutPath & TargetFile) Then
				' MsgBox("Rename of targetfile required! (coming soon)", MsgBoxStyle.Critical)
				Dim dot As Integer = TargetFile.LastIndexOf(".")
				TargetFile = TargetFile.Substring(1, dot - 1) & "_2" & TargetFile.Substring(dot)
				GoTo ren
			Else
				MoveFile(SourceFile, tmpPutPath & TargetFile, mProgress2, False)
				SizeDone += FIOFS.GetFileInfo(tmpPutPath & TargetFile).Length
			End If
			mProgress = SizeDone / TotalSize * 100
		Next
		StpClk.Stop()
		tmrProgress.Enabled = False

		mLastProgress = 0
		barProgress2.Value = 1000
		barProgressTotal.Value = 1000
		lblTimeLeft.Text = "00:00:00"
		Me.Cursor = Cursors.Default
		If chkAutoScan.Checked Then Enable(tmrAS)
		'CopyFolder(tmpGetPath, tmpPutPath, mProgress)
	End Sub

	Private Sub mnuDonate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDonate.Click
		frmDonate.Show()
	End Sub

	Private Sub DummyToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DummyToolStripMenuItem.Click
		If tmrDummy.Enabled = True Then Disable(tmrDummy, tmrProgress) : StpClk.Stop() Else Enable(tmrDummy, tmrProgress) : StpClk.Reset() : StpClk.Start()
	End Sub

	Private Sub tmrDummy_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrDummy.Tick
		mProgress += 0.33
		'If mProgress2 > 100 Then mProgress += 5 : mProgress2 = 0
		If mProgress > 100 Then Disable(tmrDummy, tmrProgress) : mProgress = 0
	End Sub

	Private Sub chkAutoScan_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAutoScan.CheckedChanged
		If chkAutoScan.Checked Then
			Disable(cmdScan)
			Enable(tmrAS)
		Else
			Disable(tmrAS)
			Enable(cmdScan)
		End If
	End Sub

	Private Sub tmrAS_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrAS.Tick
		lstDrives.BeginUpdate()
		cmdScan_Click(Nothing, Nothing)
		lstDrives.EndUpdate()
	End Sub
End Class
