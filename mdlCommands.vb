Module mdlCommands
	'Public Declare Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) As Integer
	'Public Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As IntPtr, ByVal wMsg As Integer, ByVal wParam As Integer, ByRef lParam As Object) As Integer

	Private Declare Auto Function GetWindowTextLength Lib "user32" (ByVal hWnd As Integer) As Integer

	Public Declare Auto Function GetWindowText Lib "user32" ( _
  ByVal hWnd As Integer, _
  ByVal lpString As String, _
  ByVal cch As Integer) As Integer

	Public Declare Function GetWindowThreadProcessId Lib "user32.dll" _
	(ByVal hwnd As IntPtr, _
	 ByRef lpdwProcessId As Int32) _
	 As Int32

	Public Declare Auto Function FindWindow Lib "USER32.DLL" ( _
  ByVal lpClassName As String, _
  ByVal lpWindowName As String) As Integer

	Public Declare Function SendMessage Lib "user32.dll" _
   Alias "SendMessageA" ( _
   ByVal hWnd As Integer, _
   ByVal Msg As Integer, _
   ByVal wParam As Integer, _
   ByVal lParam As Integer) As Integer

	Public Declare Function PostMessage Lib "user32.dll" _
   Alias "PostMessageA" ( _
   ByVal hWnd As Integer, _
   ByVal Msg As Integer, _
   ByVal wParam As Integer, _
   ByVal lParam As Integer) As Integer

	Public Const WM_USER As Integer = 1024
	Public Const WM_COMMAND As Integer = 273
	Public Const WM_COPYDATA As Integer = &H400
	Public Const RN As String = vbCr & vbLf
	Public ReadOnly RNA As Char() = New Char() {vbCr, vbLf}

	Public ReadOnly FIOFS As FileIO.FileSystem



	Public Class HTTP_RequestReader
		Private _Headers As New Collections.Generic.List(Of String), _HeaderValue As New Collection

		Sub New(ByVal Request As String)
			Dim sLines() As String
			sLines = Request.Replace(vbCr, "").Split(vbLf & vbLf)(0).Split(vbLf)
			For Each sLine As String In sLines
				Dim sLine2() As String = sLine.Split(":".ToCharArray(), 2)
				If StrCounter(sLine, ":") = 0 Then
					If sLine.StartsWith("GET ") Then
						_HeaderValue.Add(sLine.Split(" ")(1), "get")
						_Headers.Add("get")
					End If
				Else
					_HeaderValue.Add(sLine2(1).Trim, sLine2(0).Trim.ToLower)
					_Headers.Add(sLine2(0).Trim.ToLower)
				End If
			Next
			_Headers.Sort()
		End Sub

		Public ReadOnly Property HeaderValue(ByVal Header As String) As String
			Get
				Return _HeaderValue(Header)
			End Get
		End Property

		Public ReadOnly Property Headers() As String()
			Get
				Return _Headers.ToArray
			End Get
		End Property
	End Class


	Public Class HTTP_Sender
		Public Server As String = "HTTP Sender"
		Public Keep_Alive As String = "timeout=5, max=100"
		Public Connection As String = "Keep-Alive"
		Public Transfer_Encoding As String = "chunked"
		Public Content_Type As String = "text/html; charset=iso-8859-15"

		Public Function GetData(ByVal Data As String) As String
			Return "HTTP/1.1 200 OK" & RN & _
			"Date: " & Date.Now.ToString("R") & RN & _
			"Server: " & Server & RN & _
			"Keep-Alive: " & Keep_Alive & RN & _
			"Connection: " & Connection & RN & _
			"Transfer-Encoding: " & Transfer_Encoding & RN & _
			"Content-Type: " & Content_Type & RN & RN & "3824" & RN & Data & RN & RN
		End Function

		Public Sub GetImgData(ByVal Img As Drawing.Image, ByVal DestStream As IO.Stream, ByVal Format As Drawing.Imaging.ImageFormat)
			Dim Header As String = "HTTP/1.1 200 OK" & RN & _
			"Date: " & Date.Now.ToString("R") & RN & _
			"Server: " & Server & RN & _
			"Keep-Alive: " & Keep_Alive & RN & _
			"Connection: " & Connection & RN & _
			"Transfer-Encoding: " & Transfer_Encoding & RN & _
			"Content-Type: image/" & Format.ToString.ToLower & RN & RN & "3824" & RN
			Try
				DestStream.Write(ToByteArray(Header), 0, Header.Length)
				Img.Save(DestStream, Format)
				DestStream.Write(ToByteArray(RN & RN), 0, 4)
			Catch ex As Exception
			End Try
		End Sub
	End Class


	Public Class ColorArray
		Public Colors As Drawing.Color(,)
		Public Height, Width As Int16
		Public R, G, B, A As Long

		Public Sub New(ByVal P As Drawing.Bitmap)
			Dim x, y As Int16
			Width = P.Width
			Height = P.Height

			ReDim Colors(Width, Height)

			For x = 0 To Width - 1
				For y = 0 To Height - 1
					Colors(x, y) = P.GetPixel(x, y)
					A += Colors(x, y).A
					R += Colors(x, y).R
					G += Colors(x, y).G
					B += Colors(x, y).B
				Next
			Next
		End Sub
	End Class



	Public Function GetExeFromHWND(ByVal hWnd As IntPtr) As String
		Try
			Dim processID, threadID As Int32
			threadID = GetWindowThreadProcessId(hWnd, processID)
			Dim pid As Process = Process.GetProcessById(processID)
			Return pid.Modules(0).ModuleName
		Catch ex As Exception
			Return Nothing
		End Try
	End Function


	Public Function StringListSize(ByVal List As List(Of String), Optional ByVal SeperatorLength As Integer = 2, Optional ByVal ListStartLength As Integer = 0, Optional ByVal ListEndLength As Integer = 2) As Long
		Dim Str As String = ""
		For Each S As String In List
			Str &= S & Strings.Space(SeperatorLength)
		Next
		Return ListStartLength + Str.Length - SeperatorLength + ListEndLength
	End Function


	Public Function GetWindowTitle(ByVal hWnd As Long) As String

		' Ermittelt den Namen eines Windows anhand des
		' Window Handle

		Dim lResult As Long
		Dim sTemp As String

		'lResult = GetWindowTextLength(hWnd) + 1
		lResult = 255
		sTemp = Space(lResult)
		lResult = GetWindowText(hWnd, sTemp, lResult)
		Return Strings.Left(sTemp, Len(sTemp) - 1).Trim
		'Return sTemp.Substring(0, sTemp.Length - "- Winamp".Length)
	End Function


    Public Function MD5(ByVal Str As String) As String
        Dim Hash As Security.Cryptography.MD5 = Security.Cryptography.MD5.Create("MD5")
		Return BytesToHex(Hash.ComputeHash(ToByteArray(Str)))
    End Function


    Public Function FileMD5(ByVal File As String) As String
        Dim Hash As Security.Cryptography.MD5 = Security.Cryptography.MD5.Create("MD5")
        Dim FS As New IO.FileStream(File, IO.FileMode.Open)
		Dim Res As String = BytesToHex(Hash.ComputeHash(FS))
        FS.Close()
        Return Res
    End Function


	Public Function StrToHex(ByVal Str As String) As String
		Dim Hex As String = ""
		For Each C As Char In Str
			Hex &= Hex(Asc(C))
		Next
		Return Hex.ToUpper
	End Function


	Function UnSplit(ByVal Strings As String(), ByVal Spacer As String) As String
		Dim Result As String = ""
		For Each S As String In Strings
			Result &= S & Spacer
		Next
		Return Result.Substring(0, Result.Length - 1)
	End Function


    Public Function Limit(ByVal Number As Long, ByVal Min As Long, ByVal Max As Long) As Long
        If Number > Max Then Number = Max
        If Number < Min Then Number = Min
        Return Number
    End Function

	Public Function Limit(ByVal Number As Integer, ByVal Min As Integer, ByVal Max As Integer) As Integer
		If Number > Max Then Number = Max
		If Number < Min Then Number = Min
		Return Number
	End Function

	Public Function Limit(ByVal Number As Single, ByVal Min As Single, ByVal Max As Single) As Single
		If Number > Max Then Number = Max
		If Number < Min Then Number = Min
		Return Number
	End Function

	Public Function Limit(ByVal Number As Double, ByVal Min As Double, ByVal Max As Double) As Double
		If Number > Max Then Number = Max
		If Number < Min Then Number = Min
		Return Number
	End Function

	Public Function ToByteArray(ByVal Str As String) As Byte()
		Dim Chars As Char() = Str.ToCharArray
		Dim Bytes(Chars.GetUpperBound(0)) As Byte
		For i As Int16 = 0 To Chars.GetUpperBound(0)
			Bytes(i) = Asc(Chars(i))
		Next
		Return Bytes
	End Function


	Public Function IPtoIPAddress(ByVal IP As String) As Net.IPAddress
		If StrCounter(IP, ".") <> 3 Then Return Nothing
		Dim bIPs(3) As Byte, sIPs() As String
		sIPs = Split(IP, ".")
		bIPs(0) = sIPs(0)
		bIPs(1) = sIPs(1)
		bIPs(2) = sIPs(2)
		bIPs(3) = sIPs(3)
		Return New Net.IPAddress(bIPs)
	End Function


	Public Function BytesToString(ByVal Bytes() As Byte) As String
        Return System.Text.Encoding.ASCII.GetString(Bytes)
	End Function


	Public Function BytesToHex(ByVal Bytes() As Byte) As String
		Dim HexStr As String = ""
		For Each B As Byte In Bytes
			HexStr &= Hex(B)
		Next
		Return HexStr.ToUpper
	End Function


	Public Function StringToBytes(ByVal Text As String) As Byte()
		Return System.Text.Encoding.ASCII.GetBytes(Text)
	End Function


	Public Function CharsToString(ByVal Chars() As Char) As String
		Dim str As String = ""
		For Each chr As Char In Chars
			str &= chr
		Next
		Return str
	End Function


	Sub AutoSetPicBox(ByVal PC As Windows.Forms.PictureBox)
		If PC.Image.Width > PC.Width Or PC.Image.Height > PC.Height Then PC.SizeMode = Windows.Forms.PictureBoxSizeMode.Zoom Else PC.SizeMode = Windows.Forms.PictureBoxSizeMode.CenterImage
	End Sub


	Public Sub Break()
		Diagnostics.Debugger.Break()
	End Sub


	Public Sub LoadComboBox(ByRef CB As Windows.Forms.ComboBox, ByVal File As String, Optional ByVal ClearBefore As Boolean = True)
		If ClearBefore Then CB.Items.Clear()
		If Not FIOFS.FileExists(File) Then Exit Sub
		Dim SR As New IO.StreamReader(File), s As String
		Do Until SR.EndOfStream
			s = SR.ReadLine
			If Not CB.Items.Contains(s) Then CB.Items.Add(s)
		Loop
		SR.Close()
		SR.Dispose()
		CB.Text = CB.Items(0)
	End Sub


	Public Sub SaveComboBox(ByRef CB As Windows.Forms.ComboBox, ByVal File As String)
		Dim SW As New IO.StreamWriter(File)
		SW.WriteLine(CB.Text)
		For Each s As String In CB.Items
			SW.WriteLine(s)
		Next
		SW.Close()
		SW.Dispose()
	End Sub


	Public Function CachePic(ByVal pic As Drawing.Bitmap) As Drawing.Color(,)
		If pic Is Nothing Then Return Nothing
		Dim wid As Int16 = pic.Width
		Dim hei As Int16 = pic.Height
		Dim col(wid - 1, hei - 1) As Drawing.Color

		For x As Int16 = 0 To wid - 1
			For y As Int16 = 0 To hei - 1
				col(x, y) = pic.GetPixel(x, y)
			Next
		Next

		Return col
	End Function


	Public Function MaxColor(ByVal col As Drawing.Color) As Int16
		Dim r, g, b, a As Int16
		r = col.R
		g = col.G
		b = col.B
		a = col.A
		Return Math.Max(Math.Max(r, g), b)
	End Function


	Public Function DitherPix(ByVal CachePic As Drawing.Color(,), ByVal x As Int16, ByVal y As Int16, ByVal Pixs As String) As Drawing.Color
		Dim cR, cG, cB, cA, Count As Int16
		If Pixs = "" Then Return Nothing
		If (StrCounter(Pixs, "7")) Then
			Try
				cR += CachePic(x - 1, y - 1).R
				cG += CachePic(x - 1, y - 1).G
				cB += CachePic(x - 1, y - 1).B
				cA += CachePic(x - 1, y - 1).A
				Count += 1
			Catch ex As Exception
			End Try
		End If
		If (StrCounter(Pixs, "8")) Then
			Try
				cR += CachePic(x, y - 1).R
				cG += CachePic(x, y - 1).G
				cB += CachePic(x, y - 1).B
				cA += CachePic(x, y - 1).A
				Count += 1
			Catch ex As Exception
			End Try
		End If
		If (StrCounter(Pixs, "9")) Then
			Try
				cR += CachePic(x + 1, y - 1).R
				cG += CachePic(x + 1, y - 1).G
				cB += CachePic(x + 1, y - 1).B
				cA += CachePic(x + 1, y - 1).A
				Count += 1
			Catch ex As Exception
			End Try
		End If
		If (StrCounter(Pixs, "4")) Then
			Try
				cR += CachePic(x - 1, y).R
				cG += CachePic(x - 1, y).G
				cB += CachePic(x - 1, y).B
				cA += CachePic(x - 1, y).A
				Count += 1
			Catch ex As Exception
			End Try
		End If
		If (StrCounter(Pixs, "6")) Then
			Try
				cR += CachePic(x + 1, y).R
				cG += CachePic(x + 1, y).G
				cB += CachePic(x + 1, y).B
				cA += CachePic(x + 1, y).A
				Count += 1
			Catch ex As Exception
			End Try
		End If
		If (StrCounter(Pixs, "1")) Then
			Try
				cR += CachePic(x - 1, y + 1).R
				cG += CachePic(x - 1, y + 1).G
				cB += CachePic(x - 1, y + 1).B
				cA += CachePic(x - 1, y + 1).A
				Count += 1
			Catch ex As Exception
			End Try
		End If
		If (StrCounter(Pixs, "2")) Then
			Try
				cR += CachePic(x, y + 1).R
				cG += CachePic(x, y + 1).G
				cB += CachePic(x, y + 1).B
				cA += CachePic(x, y + 1).A
				Count += 1
			Catch ex As Exception
			End Try
		End If
		If (StrCounter(Pixs, "3")) Then
			Try
				cR += CachePic(x + 1, y + 1).R
				cG += CachePic(x + 1, y + 1).G
				cB += CachePic(x + 1, y + 1).B
				cA += CachePic(x + 1, y + 1).A
				Count += 1
			Catch ex As Exception
			End Try
		End If
		Return Drawing.Color.FromArgb(Int(cA / Count), Int(cR / Count), Int(cG / Count), Int(cB / Count))
	End Function


	Public Sub MakeGreyMap(ByVal Source As Drawing.Bitmap, ByRef Target As Drawing.Bitmap, Optional ByVal TH As Int16 = 255, Optional ByRef Progress As Single = 0)

		If Source Is Nothing Then Exit Sub
		Dim cols(,) As Drawing.Color = CachePic(Source)
		Dim pic As New Drawing.Bitmap(Source.Width, Source.Height)
		Dim col As Int16
		For x As Int16 = 0 To Source.Width - 1
			For y As Int16 = 0 To Source.Height - 1 : DoEvents()
				col = Math.Max(Math.Max(Int(cols(x, y).R), Int(cols(x, y).G)), Int(cols(x, y).B))
				If col > TH Then col = 255
				pic.SetPixel(x, y, Drawing.Color.FromArgb(255, col, col, col))
			Next
			Progress = x / (Source.Width - 1) * 100
			'If x Mod Source.Width / 8 = 0 Then
			Target = pic
			'End If
		Next
		Target = pic
	End Sub


	Public Sub ComboBoxAdd(ByRef CB As Windows.Forms.ComboBox, Optional ByVal Text As String = "")
		If Text = "" Then Text = CB.Text
		If CB.Items.Contains(Text) Then
			CB.Items.Remove(Text)
		End If
		CB.Items.Insert(0, Text)
		CB.Text = CB.Items(0)
	End Sub


	Public Function HTML_GoogleSearch(ByVal Query As String) As String
		Return "http://www.google.com/search?q=" & Query.Replace(" ", "%20") & "&hl=en"
	End Function


	Public Function HTML_GoogleSearch(ByVal Query As String, ByVal PageCount As Int16) As String()
		Dim Pages As String()
		If PageCount = 0 Then PageCount = 10
		Pages.Resize(Pages, PageCount)
		For i As Int16 = 0 To Pages.GetUpperBound(0)
			Pages(i) = "http://www.google.com/search?q=" & Query.Replace(" ", "%20") & "&hl=en&start=" & i * 10
		Next
		Return Pages
	End Function


	Public Function HTML_Link(ByVal Destination As String, Optional ByVal Text As String = "", Optional ByVal Target As String = "") As String
		If Text = "" Then Text = Destination
		If Target <> "" Then Target = """ target=""" & Target & """"
		Return "<a href=""" & Destination & Target & """>" & Text & "</a>"
	End Function


	Public Function NormalASCIIPercent(ByVal Code As String) As Single
		Code = Code.Replace(" ", "!")
		For i As Integer = 127 To 255
			Code = Code.Replace(Chr(i), " ")
		Next
		Dim x As Single = 100 - (StrCounter(Code, " ") / Code.Length * 100)
		Return x
	End Function


	Public Function CollectionInString(ByVal S As String, ByVal C As Object)
		For Each cs As String In C
			If S.Contains(cs) Then Return True
		Next
		Return False
	End Function


	Public Function ArrayItemInString(ByVal HayStack As String, ByVal NeedleList() As String, Optional ByVal CaseSensitive As Boolean = False)
		For Each s As String In NeedleList
			If CaseSensitive Then
				If HayStack.Contains(s) Then Return True
			Else
				If HayStack.ToLower.Contains(s.ToLower) Then Return True
			End If
		Next
		Return False
	End Function


	Public Function StrContainsArray(ByVal Str As String, ByVal Seek As String()) As Boolean
		For i As Int16 = 0 To Seek.GetUpperBound(0)
			If Str.Contains(Seek(i)) Then Return True
		Next
		Return False
	End Function


	Private Declare Function GetAsyncKeyState Lib "user32" (ByVal vKey As Int32) As Integer
	Public Function KeyPressed(ByVal Key As Int32) As Boolean
		KeyPressed = CBool((GetAsyncKeyState(Key) And &H8000) = &H8000)
	End Function


	Public Sub BuildDifferencePic(ByVal Pic1 As Drawing.Image, ByVal Pic2 As Drawing.Image, ByRef DiffPic As Drawing.Image)
		If Pic1.Width <> Pic2.Width Or Pic1.Height <> Pic2.Height Then Exit Sub
		Dim g As Drawing.Graphics, x, y As Integer, r1, r2, g1, g2, b1, b2 As Byte, bm1, bm2, bm3 As Drawing.Bitmap, col1, col2 As Drawing.Color

		bm1 = New Drawing.Bitmap(Pic1.Width, Pic1.Height)
		g = g.FromImage(bm1) : g.DrawImage(Pic1, 0, 0)

		bm2 = New Drawing.Bitmap(Pic1.Width, Pic1.Height)
		g = g.FromImage(bm2) : g.DrawImage(Pic2, 0, 0)

		bm3 = New Drawing.Bitmap(Pic1.Width, Pic1.Height)

		For x = 0 To Pic1.Width - 1
			For y = 0 To Pic1.Height - 1
				col1 = bm1.GetPixel(x, y) : col2 = bm2.GetPixel(x, y)
				r1 = col1.R : g1 = col1.G : b1 = col1.B	' : a1 = col1.A
				r2 = col2.R : g2 = col2.G : b2 = col2.B	' : a2 = col2.A

				If (r1 > r2) Then r1 = r1 - r2 Else r1 = r2 - r1
				If (g1 > g2) Then g1 = g1 - g2 Else g1 = g2 - g1
				If (b1 > b2) Then b1 = b1 - b2 Else b1 = b2 - b1

				bm3.SetPixel(x, y, Drawing.Color.FromArgb(255, r1, g1, b1))
			Next
		Next

		DiffPic = bm3
	End Sub


	Public Function CalcPicDifference(ByVal Pic1 As Drawing.Image, ByVal Pic2 As Drawing.Image) As Single
		Dim res As Drawing.Color(,), x, y, x2, y2, x3 As Long, diff As Single

		'res = BuildDifferenceArray(Pic1, Pic2, diff)
		Return diff

		'x2 = res.GetUpperBound(0)
		'y2 = res.GetUpperBound(1)

		'For x = 0 To x2
		'For y = 0 To y2
		'If res(x, y).A = 0 And res(x, y).R = 0 And res(x, y).G = 0 And res(x, y).B = 0 Then x3 += 1
		'diff += res(x, y).A
		'diff += res(x, y).R
		'diff += res(x, y).G
		'diff += res(x, y).B
		'Next
		'Next

		'Return diff / 4 / ((x2 + 1) * (y2 + 1)) / 255 * 100
	End Function

	Public Function BuildDifferenceArray(ByVal Colors1 As ColorArray, ByVal Colors2 As ColorArray, Optional ByRef DifferencePercent As Single = 100, Optional ByVal BlackTolerance As Int16 = 0) As Drawing.Color(,)
		If Colors1.Width <> Colors2.Width Or Colors1.Height <> Colors2.Height Then Return Nothing

		Dim r1, r2, g1, g2, b1, b2, a1, a2, x, y As Int16, Result As Drawing.Color(,)
		ReDim Result(Colors1.Width, Colors1.Height)

		For x = 0 To Colors1.Width - 1
			For y = 0 To Colors1.Height - 1
				a1 = Colors1.Colors(x, y).A : a2 = a1 - Colors2.Colors(x, y).A
				r1 = Colors1.Colors(x, y).R : r2 = r1 - Colors2.Colors(x, y).R
				g1 = Colors1.Colors(x, y).G : g2 = g1 - Colors2.Colors(x, y).G
				b1 = Colors1.Colors(x, y).B : b2 = b1 - Colors2.Colors(x, y).B

				a1 = Math.Abs(a1) : r1 = Math.Abs(r1) : g1 = Math.Abs(g1) : b1 = Math.Abs(b1)

				If (a1 - BlackTolerance <= 0) Then a1 = 0
				If (r1 - BlackTolerance <= 0) Then r1 = 0
				If (g1 - BlackTolerance <= 0) Then g1 = 0
				If (b1 - BlackTolerance <= 0) Then b1 = 0

				Result(x, y) = Drawing.Color.FromArgb(a1, r1, g1, b1)
			Next
		Next
		DifferencePercent = DifferencePercent / 4 / (Colors1.Width * Colors1.Height) / 256 * 100
		Return Result
	End Function

	Public Function BuildDifferenceArray(ByVal Pic1 As Drawing.Image, ByVal Pic2 As Drawing.Image, Optional ByRef DifferencePercent As Single = 100, Optional ByVal BlackTolerance As Int16 = 0) As Drawing.Color(,)
		If Pic1.Width <> Pic2.Width Or Pic1.Height <> Pic2.Height Then Return Nothing
		Dim g As Drawing.Graphics, x, y As Integer, r1, r2, g1, g2, b1, b2, a1, a2 As Byte, bm1, bm2 As Drawing.Bitmap, col1, col2 As Drawing.Color
		Dim result As Drawing.Color(,)

		DifferencePercent = 0

		bm1 = New Drawing.Bitmap(Pic1.Width, Pic1.Height)
		g = Drawing.Graphics.FromImage(bm1) : g.DrawImage(Pic1, 0, 0)

		bm2 = New Drawing.Bitmap(Pic1.Width, Pic1.Height)
		g = Drawing.Graphics.FromImage(bm2) : g.DrawImage(Pic2, 0, 0)

		ReDim result(0 To Pic1.Width - 1, 0 To Pic1.Height - 1)

		For x = 0 To Pic1.Width - 1
			For y = 0 To Pic1.Height - 1 : DoEvents()
				col1 = bm1.GetPixel(x, y) : col2 = bm2.GetPixel(x, y)
				r1 = col1.R : g1 = col1.G : b1 = col1.B : a1 = col1.A
				r2 = col2.R : g2 = col2.G : b2 = col2.B : a2 = col2.A

				If (r1 > r2) Then r1 = r1 - r2 Else r1 = r2 - r1
				If (g1 > g2) Then g1 = g1 - g2 Else g1 = g2 - g1
				If (b1 > b2) Then b1 = b1 - b2 Else b1 = b2 - b1
				If (a1 > a2) Then a1 = a1 - a2 Else a1 = a2 - a1

				If a1 - BlackTolerance <= 0 Then a1 = 0
				If r1 - BlackTolerance <= 0 Then r1 = 0
				If g1 - BlackTolerance <= 0 Then g1 = 0
				If b1 - BlackTolerance <= 0 Then b1 = 0

				DifferencePercent += a1
				DifferencePercent += r1
				DifferencePercent += g1
				DifferencePercent += b1

				result(x, y) = Drawing.Color.FromArgb(a1, r1, g1, b1)
			Next
		Next
		DifferencePercent = DifferencePercent / 4 / (Pic1.Width * Pic1.Height) / 256 * 100
		Return result
	End Function

	Private Class blub
		Public checked As Boolean = False
		Public text As String = ""

		Public Sub New(ByVal t As String)
			text = t
			checked = False
		End Sub
	End Class

	Public Function GetEliminatorCount(ByVal Size As Long) As Long
		Dim i As Long, blib As New List(Of blub)

		For i = 1 To Size
			blib.Add(New blub(i))
		Next

		i = 0
		For Each blab As blub In blib
			For Each blob As blub In blib
				If Not blab.Equals(blob) Or Not blob.checked Then i += 1
			Next
			blab.checked = True
		Next

		Return i
	End Function

	Public Function CountPixelAmount(ByVal Pic As Drawing.Bitmap, ByVal Col As Drawing.Color)
		Dim x, y As Integer, col2 As Drawing.Color, count As Integer = 0
		Col = Drawing.Color.FromArgb(255, Col.R, Col.G, Col.B)
		For x = 0 To Pic.Width - 1
			For y = 0 To Pic.Height - 1
				col2 = Pic.GetPixel(x, y)
				col2 = Drawing.Color.FromArgb(255, col2.R, col2.G, col2.B)
				If Col = col2 Then count += 1
			Next
		Next
		Return count
	End Function

	Public Function CountPixelAmount(ByVal Pic As Drawing.Bitmap, ByVal AwayFromBlack As Byte)
		Dim x, y As Integer, col2 As Drawing.Color, count As Integer = 0
		For x = 0 To Pic.Width - 1
			For y = 0 To Pic.Height - 1
				col2 = Pic.GetPixel(x, y)
				col2 = Drawing.Color.FromArgb(255, col2.R, col2.G, col2.B)
				If (col2.R <= AwayFromBlack And col2.G <= AwayFromBlack And col2.B <= AwayFromBlack) Then count += 1
			Next
		Next
		Return count
	End Function

	Public Sub Print2(ByVal FileNumber As Byte, ByVal Param() As Object)
		Dim Data As String = "", i As Object
		For Each i In Param
			Data &= i & ","
		Next
		Data = Data.Substring(Data.Length - 1)
		Print(FileNumber, Data)
	End Sub

	Public Function IsMultipleOf(ByVal Number, ByVal Base) As Boolean
		If Number / Base = Int(Number / Base) Then Return True Else Return False
	End Function

	Public clsKeyDelimiter As String = "<{|#|}>"
	Class clsKeyValue
		Public Key As String
		Public Value As Object

		Public Sub New(ByVal V As Object, ByVal K As String)
			Value = V
			Key = K
		End Sub
	End Class

	Function GetDistance(ByVal x As Single, ByVal y As Single) As Double
		Return Math.Sqrt((x * x) + (y * y))
	End Function

	Function DegreeToRadians(ByVal degrees As Single) As Single
		Do While degrees > 180
			degrees -= 360
		Loop
		Do While degrees < -180
			degrees += 360
		Loop
		Return Math.PI / 180 * degrees
	End Function

	Sub AddRotation(ByVal x As Single, ByVal y As Single, ByVal dir As Single, ByRef nx As Single, ByRef ny As Single)
		Dim dist As Double = GetDistance(x, y), myrot As Double = dir + 90, acos As Double
		myrot = DegreeToRadians(myrot)
		acos = y / dist
		If Single.IsNaN(acos) Then nx = 0 : ny = 0 : Exit Sub
		If x < 0 Then
			myrot -= Math.Acos(acos)
		Else
			myrot += Math.Acos(acos)
		End If
		nx = Math.Round(Math.Cos(myrot) * dist, 8)
		ny = Math.Round(Math.Sin(myrot) * dist, 8)
	End Sub

	Function LeaveNumbers(ByVal Text As String) As Double
		Dim i As Long, newText As String = ""
		For i = 0 To Text.Length - 1
			Select Case Text.Substring(i, 1)
				Case 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 : newText += Text.Substring(i, 1) : Exit Select
				Case Else
			End Select
		Next
		Return newText
	End Function

	Public Sub AddKeyValue(ByRef Col As Collection, ByVal Value As Object, ByVal Key As String)
		Dim tmpKV As clsKeyValue
		tmpKV = New clsKeyValue(Value, Key)
		Col.Add(tmpKV, Key)
	End Sub

	Function GetDistance(ByVal x1 As Single, ByVal y1 As Single, ByVal x2 As Single, ByVal y2 As Single) As Single
		Dim sideA As Single, sideB As Single, sideC As Single
		sideA = x2 - x1
		sideB = y1 - y2
		sideC = Math.Sqrt(sideA ^ 2 + sideB ^ 2)
		Return sideC
	End Function

	Function GetDirInDegree(ByVal x1 As Single, ByVal y1 As Single, ByVal x2 As Single, ByVal y2 As Single) As Single
		Dim sideA As Single, sideB As Single, Sdir As Single
		sideA = x2 - x1
		sideB = y1 - y2
		Sdir = DegreeDirection(sideA, sideB) - 90
		If Sdir < 0 Then Sdir = 360 + Sdir
		Return Sdir
	End Function

	Function Percent(ByVal Value As Single, ByVal MaxValue As Single) As Single
		Return Value / MaxValue * 100
	End Function

	Function MatchList(ByVal Text As String, ByVal Mask As String) As List(Of String)
		Dim Match As String
		MatchList = New List(Of String)
		If StrCounter(Mask, "*") = 1 Then
			Dim MaskL As String = Mask.Split("*")(0)
			Dim MaskR As String = Mask.Split("*")(1)
			Do While Text.Contains(MaskL)
				Match = Text.Substring(Text.IndexOf(MaskL) + MaskL.Length)
				Text = Match
				Match = Match.Split(New String() {MaskR}, StringSplitOptions.RemoveEmptyEntries)(0)
				MatchList.Add(Match)
			Loop
		Else
			Break()
		End If
		Return MatchList
	End Function

	Function RotCos(ByVal Dir As Integer) As Single
		Return Math.Cos(Dir * Math.PI / 180)
	End Function

	Function RotSin(ByVal Dir As Integer) As Single
		Return Math.Sin(Dir * Math.PI / 180)
	End Function

	Function DegreeDirection(ByVal X As Double, ByVal Y As Double) As Double
		Return Math.Atan2(X, Y) * (180 / Math.PI) + 90
	End Function

	Function GetFirstNothing(ByRef aArray() As Object) As Long
		Dim i
		For i = 0 To UBound(aArray)
			If aArray(i) Is Nothing Then Return i
		Next
		Return -1
	End Function

	Sub CenterControl(ByRef ParentControl As Windows.Forms.Control, ByRef Control As Windows.Forms.Control)
		Dim x, y, x2, y2
		x = ParentControl.Width / 2
		y = ParentControl.Height / 2

		x2 = x - (Control.Width / 2)
		y2 = y - (Control.Height / 2)

		Control.Left = x2
		Control.Top = y2
	End Sub

	Function ArrayContains(ByVal A As Array, ByVal Find As Object)
		For Each X As Object In A
			If X = Find Then Return True
		Next
		Return False
	End Function

	Sub Sleep(ByVal MilliSeconds As Long)
		System.Threading.Thread.Sleep(MilliSeconds)
	End Sub

	Sub Wait(ByVal MilliSeconds As Long)
		Dim StpClk As New Stopwatch
		StpClk.Start()
		While StpClk.ElapsedMilliseconds < MilliSeconds : DoEvents() : Sleep(10) : End While
		StpClk.Stop()
	End Sub

	Sub ScanFilesIntoArray(ByVal Path As String, ByRef FArray() As String)
		Dim f() As String = System.IO.Directory.GetFiles(Path, "*.*")
		FArray = f
	End Sub

	Function LoadImage(ByVal PicFile As String) As Drawing.Bitmap
		Dim oStream As New System.IO.FileStream(PicFile, IO.FileMode.Open)
		Dim oBitmap As Drawing.Bitmap
		oBitmap = New Drawing.Bitmap(oStream)
		oStream.Close()
		LoadImage = oBitmap
	End Function

	Public Sub GetPicSize(ByVal Pic As String, ByRef Width As Integer, ByRef Height As Integer)
		If Not FIOFS.FileExists(Pic) Then Exit Sub
		On Error Resume Next
		Dim oStream As New System.IO.FileStream(Pic, IO.FileMode.Open)
		Dim oBitmap As Drawing.Bitmap
		oBitmap = New Drawing.Bitmap(oStream)
		oStream.Close()
		Width = oBitmap.Width
		Height = oBitmap.Height
	End Sub

	Public Function AutoSizeImage(ByVal oBitmap As Drawing.Image, ByVal maxWidth As Integer, ByVal maxHeight As Integer, _
	 Optional ByVal bStretch As Boolean = False, Optional ByVal Mode As Drawing.Drawing2D.InterpolationMode = Drawing.Drawing2D.InterpolationMode.HighQualityBicubic) As Drawing.Image

		If oBitmap Is Nothing Then Return Nothing
		' Gr��enverh�ltnis der max. Dimension
		Dim maxRatio As Single = maxWidth / maxHeight

		' Bildgr��e und aktuelles Gr��enverh�ltnis
		Dim imgWidth As Integer = oBitmap.Width
		Dim imgHeight As Integer = oBitmap.Height
		Dim imgRatio As Single = imgWidth / imgHeight

		' Bild anpassen?
		If (imgWidth > maxWidth Or imgHeight > maxHeight) Or (bStretch) Then
			If imgRatio <= maxRatio Then
				' Gr��enverh�ltnis des Bildes ist kleiner als die
				' maximale Gr��e, in der das Bild angezeigt werden kann.
				' In diesem Fall muss die Bildbreite angepasst werden.
				imgWidth = imgWidth / (imgHeight / maxHeight)
				imgHeight = maxHeight
			Else
				' Gr��enverh�ltnis des Bildes ist gr��er als die 
				' maximale Gr��e, in der das Bild angezeigt werden kann.
				' In diesem Fall muss die Bildh�he angepasst werden.
				imgHeight = imgHeight / (imgWidth / maxWidth)
				imgWidth = maxWidth
			End If

			' Bitmap-Objekt in der neuen Gr��e erstellen
			Dim oImage As New Drawing.Bitmap(imgWidth, imgHeight)

			' Bild interpolieren, damit die Qualit�t erhalten bleibt
			Using g As Drawing.Graphics = Drawing.Graphics.FromImage(oImage)
				g.InterpolationMode = Mode
				g.DrawImage(oBitmap, New Drawing.Rectangle(0, 0, imgWidth, imgHeight))
			End Using

			' neues Bitmap zur�ckgeben
			Return oImage
		Else
			' unver�ndertes Originalbild zur�ckgeben
			Return oBitmap
		End If
	End Function

	Function AppPath() As String
		AppPath = My.Application.Info.DirectoryPath & "\"
	End Function

	Function AppPathExe() As String
		AppPathExe = AppPath() & My.Application.Info.AssemblyName
	End Function

	Function MakeTimeString(Optional ByVal Hours As Single = 0, Optional ByVal Minutes As Single = 0, Optional ByVal Seconds As Single = 0) As String
		Dim mSeconds As Double, mH As Single, mM As Single, mS As Single
		mSeconds = (Hours * 3600) + (Minutes * 60) + Seconds
		mH = Int(mSeconds / 3600)
		mSeconds -= (mH * 3600)
		mM = Int(mSeconds / 60)
		mSeconds -= (mM * 60)
		mS = Int(mSeconds)
		Return Format(mH, "00") & ":" & Format(mM, "00") & ":" & Format(mS, "00")
	End Function

    Function GenerateHASH(ByVal Filename As String, Optional ByVal ByteCount As Integer = 4 * 1024) As String
        Dim UseFile As Byte, i As Integer, FileLength As Object, BufferByte As String, HashByte As String
        BufferByte = " "
        GenerateHASH = ""
        If FileIO.FileSystem.FileExists(Filename) = False Then Exit Function
        UseFile = FreeFile()
        FileOpen(UseFile, Filename, OpenMode.Binary, OpenAccess.Read)
        FileLength = FileIO.FileSystem.GetFileInfo(Filename).Length
        If FileLength < ByteCount * 2 Then
            For i = 1 To FileLength / 2 : DoEvents()
                FileGet(UseFile, BufferByte, i)
                HashByte = Hex(Asc(BufferByte))
                GenerateHASH += HashByte
            Next
        Else
            For i = 1 To ByteCount : DoEvents()
                FileGet(UseFile, BufferByte, ((i - 1) * Int(FileLength / ByteCount)) + 1)
                HashByte = Hex(Asc(BufferByte))
                If Len(HashByte) = 1 Then HashByte = 0 & HashByte
                BufferByte = " "
                GenerateHASH += HashByte
            Next i
        End If
        FileClose(UseFile)
    End Function

	Sub WriteINI(ByVal INIFile As String, ByVal Section As String, ByVal Item As String, ByVal Value As String)
		Dim colINI As New Collection, i As Object, x As Object, SectionFound As Boolean, count As Integer
		If INIFile = "" Then INIFile = AppPathExe() + ".ini"
		If StrCounter(Right(INIFile, 5), ".") = 0 Then INIFile = INIFile & ".ini"
		'UseFile = FreeFile()
		'If IO.File.Exists(INIFile) = True Then
		'FileOpen(UseFile, INIFile, OpenMode.Input)
		'Do Until EOF(UseFile)
		'blib = LineInput(UseFile)
		'If Left(blib, 1) = """" Then blib = Right(blib, Len(blib) - 1)
		'If Right(blib, 1) = """" Then blib = Left(blib, Len(blib) - 1)
		'colINI.Add(blib)
		'Console.WriteLine(blib)
		'Loop
		'FileClose(UseFile)
		'End If

		If Left(Section, 1) <> "[" Then Section = "[" & Section
		If Right(Section, 1) <> "]" Then Section = Section & "]"
		LoadCollectionFile(colINI, INIFile)
		KillDoubleCollectionEntrys(colINI)
		count = 0
		SectionFound = False
		For Each i In colINI
			count += 1
			If i = Section Then
				For x = count To colINI.Count
					If x > colINI.Count Then Exit For
					If StrSplitter(colINI(x), "=") = Item Then
						colINI.Remove(x)
						x -= 1
						If x = colINI.Count Then Exit For
					End If
				Next
				colINI.Add(Item & "=" & Value, , , count) : SectionFound = True : Exit For
			End If
		Next
		If SectionFound = False Then colINI.Add(Section) : colINI.Add(Item & "=" & Value)
		SaveCollectionFile(colINI, INIFile, False)

		'FileOpen(UseFile, INIFile, OpenMode.Output)
		'For Each i In colINI
		'If Left(i, InStr(i, "=") - 1) = Left(Value, InStr(Value, "=") - 1) Then
		'PrintLine(UseFile, i)
		'End If
		'Next
		'PrintLine(UseFile, Item & "=" & Value)
		'FileClose(UseFile)
	End Sub

	Sub KillDoubleCollectionEntrys(ByRef Col As Collection)
		Dim col1 As Collection, col2 As New Collection, i As Object
		col1 = Col
		For Each i In col1
			If IsInCollection(col2, i, True) = False Then col2.Add(i)
		Next
		Col = col2
	End Sub

	Function ReadINI(ByVal INIFile As String, ByVal Section As String, ByVal Item As String, Optional ByVal DefaultValue As Object = "") As String
		If INIFile = "" Then INIFile = AppPathExe() + ".ini"
		Dim colINI As New Collection, i As Object, SectionFound As Boolean
		If Left(Section, 1) <> "[" Then Section = "[" & Section
		If Right(Section, 1) <> "]" Then Section = Section & "]"
		If StrCounter(Right(INIFile, 5), ".") = 0 Then INIFile = INIFile & ".ini"
		LoadCollectionFile(colINI, INIFile)
		SectionFound = False
		For Each i In colINI
			If i = Section Then SectionFound = True
			If StrSplitter(i, "=") = Item And SectionFound = True Then
				ReadINI = StrSplitter(i, "=", 2)
				Exit Function
			End If
		Next
		ReadINI = DefaultValue
	End Function

	Function IsClsKeyVal(ByVal Entry As Object) As Boolean
		Dim Key As String
		Try
			Key = Entry.Key
			If Key <> "" Then Return True
		Catch ex As Exception
			If ex.Message = "System.MissingMemberException" Then Return False
		End Try
		Return False
	End Function

	Sub SaveCollectionFile(ByVal cCol As Collection, ByVal SCFile As String, Optional ByVal Append As Boolean = False)
		Dim UseFile As Byte, tmpCol As New Collection, tmp As Object, i As Object, x As Object
		Dim tmpKey As String, tmpVal As Object
		If StrCounter(Right(SCFile, 5), ".") = 0 Then SCFile = SCFile & ".vbcf"
		UseFile = FreeFile()
		If IO.File.Exists(SCFile) Then
			If Append = True Then
				FileOpen(UseFile, SCFile, OpenMode.Input)
				Do Until EOF(UseFile)
					tmp = LineInput(UseFile)
					If tmp <> "" Then
						If StrCounter(tmp, clsKeyDelimiter) > 0 Then
							tmpKey = StrSplitter(tmp, clsKeyDelimiter)
							tmpVal = StrSplitter(tmp, clsKeyDelimiter, 2)
							tmpCol.Add(tmpKey & clsKeyDelimiter & tmpVal)
						Else
							tmpCol.Add(tmp)
						End If
					End If
				Loop
				FileClose(UseFile)
			Else
				Kill(SCFile)
			End If
		End If
		FileOpen(UseFile, SCFile, OpenMode.Output)
		If cCol.Count > 0 Then
			x = 0
			For Each i In cCol : DoEvents()
				x = x + 1
				If IsClsKeyVal(i) Then
					tmpKey = i.Key
					tmpVal = i.Value
					tmpCol.Add(tmpKey & clsKeyDelimiter & tmpVal)
				Else
					tmpCol.Add(i)
				End If
			Next
		End If
		KillDoubleCollectionEntrys(cCol)
		For Each i In cCol : DoEvents()
			If IsClsKeyVal(i) Then
				tmpKey = i.key
				tmpVal = i.value
				i = tmpKey & clsKeyDelimiter & tmpVal
			End If
			PrintLine(UseFile, i)
		Next
		FileClose(UseFile)
	End Sub

	Sub LoadCollectionFile(ByRef cCol As Collection, ByVal SCFile As String)
		Dim UseFile As Byte, tmp As String, tmpkey As String, tmpval As Object
		If StrCounter(Right(SCFile, 5), ".") = 0 Then SCFile = SCFile & ".vbcf"
		UseFile = FreeFile()
		If IO.File.Exists(SCFile) Then
			Try
				FileOpen(UseFile, SCFile, OpenMode.Input)
			Catch ex As Exception
				If ex.Message <> "<>" Then Stop
			End Try
			Do Until EOF(UseFile)
				tmp = LineInput(UseFile)
				If tmp <> "" Then
					If StrCounter(tmp, clsKeyDelimiter) > 0 Then
						tmpkey = StrSplitter(tmp, clsKeyDelimiter)
						tmpval = StrSplitter(tmp, clsKeyDelimiter, 2)
						AddKeyValue(cCol, tmpval, tmpkey)
					Else
						cCol.Add(tmp)
					End If
				End If
			Loop
			FileClose(UseFile)
		End If
	End Sub

	Sub SaveListFile(ByVal cCol As List(Of String), ByVal SCFile As String, Optional ByVal Append As Boolean = False)
		Dim UseFile As Byte, tmpCol As New Collection, tmp As Object, i As Object, x As Object
		Dim tmpKey As String, tmpVal As Object
		If StrCounter(Right(SCFile, 5), ".") = 0 Then SCFile = SCFile & ".vbcf"
		UseFile = FreeFile()
		If IO.File.Exists(SCFile) Then
			If Append = True Then
				FileOpen(UseFile, SCFile, OpenMode.Input)
				Do Until EOF(UseFile)
					tmp = LineInput(UseFile)
					If tmp <> "" Then
						tmpCol.Add(tmp)
					End If
				Loop
				FileClose(UseFile)
			Else
				Kill(SCFile)
			End If
		End If
		FileOpen(UseFile, SCFile, OpenMode.Output)
		If cCol.Count > 0 Then
			x = 0
			For Each i In cCol : DoEvents()
				x = x + 1
				tmpCol.Add(i)
			Next
		End If
		KillDoubleListEntrys(cCol)
		For Each i In cCol : DoEvents()
			PrintLine(UseFile, i)
		Next
		FileClose(UseFile)
	End Sub

	Sub LoadListFile(ByRef cCol As List(Of String), ByVal SCFile As String)
		Dim UseFile As Byte, tmp As String, tmpkey As String, tmpval As Object
		If StrCounter(Right(SCFile, 5), ".") = 0 Then SCFile = SCFile & ".vbcf"
		UseFile = FreeFile()
		If IO.File.Exists(SCFile) Then
			Try
				FileOpen(UseFile, SCFile, OpenMode.Input)
			Catch ex As Exception
				If ex.Message <> "<>" Then Stop
			End Try
			Do Until EOF(UseFile)
				tmp = LineInput(UseFile)
				If tmp <> "" Then
					cCol.Add(tmp)
				End If
			Loop
			FileClose(UseFile)
		End If
	End Sub

	Sub KillDoubleListEntrys(ByRef Col As List(Of String))
		Dim col1 As List(Of String), col2 As New List(Of String), i As Object
		col1 = Col
		For Each i In col1
			If IsInCollection(col2, i, True) = False Then col2.Add(i)
		Next
		Col = col2
	End Sub

	Sub MoveFolder(ByVal SourcePath As String, ByVal TargetPath As String, Optional ByRef Progress As Single = 0, Optional ByRef Progress2 As Single = 0, Optional ByVal OverWrite As Boolean = False)
		Dim cFiles, i As Object, AddPath As String, tmpTarget As String, Hash1 As String, Hash2 As String, PC As Long
		Dim TotalSize As Long = 0, SizeDone As Long
		cFiles = FIOFS.GetFiles(SourcePath)
		For Each f As String In cFiles
			TotalSize += FIOFS.GetFileInfo(f).Length
		Next
		On Error Resume Next
		For Each i In cFiles : DoEvents()
			PC = PC + 1
			AddPath = StrSplitter(i, "\", StrCounter(SourcePath, "\") + 2)
			AddPath = StrSplitter(AddPath, "\", , StrCounter(AddPath, "\"))
			tmpTarget = TargetPath & "\" & AddPath & "\"
			FileSystem.MkDir(tmpTarget)
			tmpTarget = tmpTarget & StrSplitter(i, "\", 32765)
			MoveFile(i, tmpTarget, Progress2, OverWrite)
			'FileIO.FileSystem.CopyFile(i, tmpTarget)
			'Hash1 = HashFile(i)
			'Hash2 = HashFile(tmpTarget)
			'If Hash1 = Hash2 Then My.Computer.FileSystem.DeleteFile(i)
			'Progress = PC / cFiles.Count * 100
			SizeDone += FIOFS.GetFileInfo(i).Length
			Progress = SizeDone / TotalSize * 100
		Next i
	End Sub

	Sub CopyFolder(ByVal SourcePath As String, ByVal TargetPath As String, Optional ByRef Progress As Single = 0, Optional ByRef Progress2 As Single = 0, Optional ByVal OverWrite As Boolean = False) ', Optional ByRef FileProgress As Single = 0)
		Dim cFiles, i As Object, AddPath As String, tmpTarget As String, Hash1 As String, Hash2 As String, PC As Long
		cFiles = FIOFS.GetFiles(SourcePath)
		On Error Resume Next
		For Each i In cFiles : DoEvents()
			PC = PC + 1
			AddPath = StrSplitter(i, "\", StrCounter(SourcePath, "\") + 2, StrCounter(i, "\") - 1 - StrCounter(SourcePath, "\"))
			'AddPath = StrSplitter(AddPath, "\", , StrCounter(AddPath, "\"))
			tmpTarget = TargetPath & "\" & AddPath & "\"
			FileSystem.MkDir(tmpTarget)
			tmpTarget = tmpTarget & StrSplitter(i, "\", 32765)
			CopyFile(i, tmpTarget, Progress2, OverWrite)
			Progress = PC / cFiles.Count * 100
		Next i
	End Sub

    Public Enum FileHashType
        None = -1
        MD5 = 0
        Fast256 = 1
    End Enum

    Sub CopyFile(ByVal SourceFile As String, ByVal TargetFile As String, Optional ByRef Progress As Single = 0, Optional ByVal OverWrite As Boolean = False, Optional ByVal HashType As FileHashType = FileHashType.MD5)
        Dim CurrentByte As Long, FileSize As Object, Buffer((64 * 1024) - 1) As Byte, tmpTarget As String, BufferLength As Integer
        CurrentByte = 0
        FileSize = FileLen(SourceFile)

		If IO.File.Exists(TargetFile) = True Then
			If OverWrite = True Then My.Computer.FileSystem.DeleteFile(TargetFile) Else Exit Sub
		End If

        Dim Hash1 As String, Hash2 As String
        Select Case HashType
            Case FileHashType.Fast256 : Hash1 = HashFile(SourceFile)
            Case FileHashType.MD5 : Hash1 = FileMD5(SourceFile)
            Case FileHashType.None : Hash1 = ""
            Case Else : Hash1 = FileMD5(SourceFile)
        End Select

        tmpTarget = StrSplitter(TargetFile, "\", , StrCounter(TargetFile, "\"))
		IO.Directory.CreateDirectory(tmpTarget)

		If FileSize < 2 * 1024 * 1024 Then
			FIOFS.CopyFile(SourceFile, TargetFile, True)
		Else
			Dim FS1 As New IO.FileStream(SourceFile, IO.FileMode.Open, IO.FileAccess.Read)
			Dim FS2 As New IO.FileStream(TargetFile, IO.FileMode.Create, IO.FileAccess.Write)

			CurrentByte = 0
			Do : DoEvents()
				BufferLength = Buffer.Length
				'Array.Clear(Buffer, 0, Buffer.Length)
				Try
					BufferLength = FS1.Read(Buffer, 0, Buffer.Length)
				Catch ex As Exception
					MsgBox("blub")
				End Try
				CurrentByte += Buffer.Length
				'If CurrentByte > FileSize Then BufferLength = CurrentByte - FileSize
				FS2.Write(Buffer, 0, BufferLength)
				Progress = CurrentByte / FileSize * 100
				If Progress > 100 Then Progress = 100
			Loop Until CurrentByte >= FileSize
			FS1.Close()
			FS2.Close()
		End If

        Try
            FIOFS.GetFileInfo(TargetFile).CreationTime = FIOFS.GetFileInfo(SourceFile).CreationTime
            FIOFS.GetFileInfo(TargetFile).LastWriteTime = FIOFS.GetFileInfo(SourceFile).LastWriteTime
            FIOFS.GetFileInfo(TargetFile).LastAccessTime = FIOFS.GetFileInfo(SourceFile).LastAccessTime

            Select Case HashType
                Case FileHashType.Fast256 : Hash2 = HashFile(TargetFile)
                Case FileHashType.MD5 : Hash2 = FileMD5(TargetFile)
                Case FileHashType.None : Hash2 = ""
                Case Else : Hash2 = FileMD5(TargetFile)
            End Select

            If Hash1 <> Hash2 Then
                My.Computer.FileSystem.DeleteFile(TargetFile)
                Debug.Print("CopyFile: Hash's of " & SourceFile & " didn't match, deleted TargetFile.")
            End If
        Catch ex As Exception
            My.Computer.FileSystem.DeleteFile(TargetFile)
            Debug.Print("CopyFile: Access Error, deleted TargetFile.")
        End Try
    End Sub

    Sub MoveFile(ByVal SourceFile As String, ByVal TargetFile As String, Optional ByRef Progress As Single = 0, Optional ByVal OverWrite As Boolean = False, Optional ByVal UseFastHash As Boolean = False)
        Dim Hash1 As String, Hash2 As String
        CopyFile(SourceFile, TargetFile, Progress, OverWrite, UseFastHash)
        If (UseFastHash) Then
            Hash1 = HashFile(SourceFile)
            Hash2 = HashFile(TargetFile)
        Else
            Hash1 = FileMD5(SourceFile)
            Hash2 = FileMD5(TargetFile)
        End If
        If FIOFS.GetFileInfo(SourceFile).IsReadOnly Then FIOFS.GetFileInfo(SourceFile).IsReadOnly = False
		If Hash1 = Hash2 Then Kill(SourceFile) 'My.Computer.FileSystem.DeleteFile(SourceFile)
    End Sub

	Function HashFile(ByVal FileName As String) As String
		Dim UseFile As Byte, Buffer As String = Space(128), i As Object, FileLenght As Long, FileScanParts As Integer
        FileScanParts = 256
		FileLenght = FileLen(FileName)
		HashFile = ""
		UseFile = FreeFile()
		FileOpen(UseFile, FileName, OpenMode.Binary, OpenAccess.Read, OpenShare.LockWrite)
		For i = 1 To FileScanParts : DoEvents()
			FileGet(UseFile, Buffer, ((FileLenght / FileScanParts * (i - 1)) + 1), True)
			HashFile += HashString(Buffer)
		Next
		FileClose(UseFile)
	End Function

	Function HashString(ByVal Text As String) As String
		Dim Buffer As Long, tmpHash, i As Object
		tmpHash = ""
		For i = 1 To Len(Text)
			If i / 8 = Int(i / 8) Then tmpHash = tmpHash & Hex(Buffer) : Buffer = 0
			Buffer += Asc(Mid(Text, i, 1))
		Next
		tmpHash += Hex(Buffer)
		HashString = tmpHash
	End Function

	Function StrSplitter(ByVal Text As String, Optional ByVal SplitChar As String = "|", Optional ByVal SplitNum As Integer = 1, Optional ByVal SplitCount As Integer = 1) As String
		Dim Splits As Array, i As Object, MaxSplits As Integer, SplitStart As Long
		StrSplitter = ""
		Splits = Split(Text, SplitChar, -1)	'SplitNum + SplitCount + 1
		If SplitNum = 0 Then
			SplitNum = UBound(Splits)
			If SplitCount = 1 Then StrSplitter = Splits(SplitNum) : Exit Function
			If SplitCount > 1 Then
				StrSplitter = ""
				For i = SplitNum - SplitCount + 1 To SplitNum
					StrSplitter &= Splits(i) & SplitChar
				Next
				StrSplitter = Left(StrSplitter, Len(StrSplitter) - 1)
			End If
		End If
		If SplitNum < 0 Then SplitNum = SplitNum + StrCounter(Text, SplitChar) + 1
		If SplitCount = 0 Then Exit Function
		If SplitCount < 0 Then SplitCount = SplitCount + StrCounter(Text, SplitChar) + 1
		MaxSplits = UBound(Splits) + 1	'StrCounter(Text, SplitChar) + 1
		'Console.WriteLine(MaxSplits)
		If SplitNum + SplitCount - 1 > MaxSplits Then SplitNum = MaxSplits - SplitCount + 1
		'If SplitCount + SplitNum - 1 > MaxSplits - SplitNum + 1 Then SplitCount = MaxSplits - SplitNum + 1
		On Error Resume Next
		StrSplitter = ""
		If SplitCount = 1 Then
			StrSplitter = Splits(SplitNum - 1)
		Else
			For i = SplitStart To SplitStart + SplitCount - 1
				DoEvents()
				StrSplitter = StrSplitter & Splits(SplitNum + i - 1)
				If i < SplitCount - 1 Then StrSplitter = StrSplitter & SplitChar
			Next
		End If
	End Function

	Function StrCounter(ByVal Text As String, ByVal Find As String) As Integer
		Try
			'Dim a As Char() = Text.Replace(Nothing, "").ToCharArray
			If Text.Length > 1024 * 128 Then Return 0
			Return UBound(Split(Text, Find))
		Catch
		End Try
	End Function

	Function StringIndex(ByVal Text As String, ByVal Find As Char) As Collections.Generic.List(Of Long)
		Dim l As New List(Of Long)
		For i As Long = 0 To Text.Length - 1
			If Text(i) = Find Then l.Add(i)
		Next
		Return l
	End Function

	Sub CalcPicSize(ByVal PicW As Integer, ByVal PicH As Integer, ByVal MaxPicW As Integer, ByVal MaxPicH As Integer, ByRef NewPicW As Integer, ByRef NewPicH As Integer)
		Dim Ratio As Single, Ratio2 As Single
		Ratio = PicW / PicH
		Ratio2 = MaxPicW / MaxPicH
		If Ratio >= Ratio2 Then
			NewPicW = MaxPicW
			NewPicH = NewPicW / Ratio
		Else
			NewPicH = MaxPicH
			NewPicW = NewPicH * Ratio
		End If
		'Debug.Print(Ratio & " - " & Ratio2 & " - " & NewPicW & " - " & NewPicH)
	End Sub

	Sub RecycleFolder(ByVal Folder As String)
		My.Computer.FileSystem.DeleteDirectory(Folder, FileIO.UIOption.OnlyErrorDialogs, FileIO.RecycleOption.SendToRecycleBin)
	End Sub

	Sub RecycleFile(ByVal File As String)
		My.Computer.FileSystem.DeleteFile(File, FileIO.UIOption.OnlyErrorDialogs, FileIO.RecycleOption.SendToRecycleBin)
	End Sub

	Sub DoEvents()
		Windows.Forms.Application.DoEvents()
	End Sub

	Sub Enable(ByRef Ob1 As Object, Optional ByRef Ob2 As Object = "", Optional ByRef Ob3 As Object = "", Optional ByRef Ob4 As Object = "", Optional ByRef Ob5 As Object = "")
		On Error Resume Next
		Ob1.enabled = True
		Ob2.enabled = True
		Ob3.enabled = True
		Ob4.enabled = True
		Ob5.enabled = True
	End Sub

	Sub Disable(ByRef Ob1 As Object, Optional ByRef Ob2 As Object = "", Optional ByRef Ob3 As Object = "", Optional ByRef Ob4 As Object = "", Optional ByRef Ob5 As Object = "")
		On Error Resume Next
		Ob1.enabled = False
		Ob2.enabled = False
		Ob3.enabled = False
		Ob4.enabled = False
		Ob5.enabled = False
	End Sub

	Function IsInCollection(ByRef Col As Object, ByVal TextMask As String, Optional ByVal ExactMatch As Boolean = False) As Boolean
		Dim i As Object
		If ExactMatch = False Then
			For Each i In Col
				If StrCounter(i, TextMask) > 0 Then Return True
			Next
		Else
			'For Each i In Col
			'If i = TextMask Then Return True
			'Next
			Return Col.Contains(TextMask)
		End If
	End Function

	Function AppVersion() As String
		Dim Version As String, Ver1 As String, Ver2 As String
		Version = My.Application.Info.Version.Build
		Ver1 = StrSplitter(Version, ".")
		Ver2 = StrSplitter(Version, ".", 4)
		AppVersion = Ver1 + Int(Ver2 / 100) & "." & Ver2 - (Int(Ver2 / 100) * 100)
		If Right(AppVersion, 2) = ".0" Then AppVersion = Left(AppVersion, Len(AppVersion) - 2)
	End Function

	Function LoadFile_(ByVal FileName As String) As String
		Dim UseFile As Byte, Buffer As String
		Buffer = Space(256 * 1024)
		LoadFile_ = ""
		If FileIO.FileSystem.FileExists(FileName) = True Then
			UseFile = FreeFile()
			FileOpen(UseFile, FileName, OpenMode.Binary)
			Do Until EOF(UseFile) = True
				FileGet(UseFile, Buffer)
				LoadFile_ &= Buffer
			Loop
			FileClose(UseFile)
		End If
	End Function

	Public Function LoadFile(ByVal File As String) As String
		Try
			Dim sr As New IO.StreamReader(File)
			LoadFile = sr.ReadToEnd()
			sr.Close()
		Catch ex As Exception
		End Try
	End Function

	Sub SaveFile(ByVal FileName As String, ByVal Text As String, Optional ByVal Overwrite As Boolean = False)
		Dim UseFile As Byte

		If Overwrite = True Then
			If FileIO.FileSystem.FileExists(FileName) = True Then Kill(FileName)
		End If

		If FileIO.FileSystem.FileExists(FileName) = False Then
			UseFile = FreeFile()
			FileOpen(UseFile, FileName, OpenMode.Binary)
			FilePut(UseFile, Text)
			FileClose(UseFile)
		End If
	End Sub

	Function RemoveFromTo(ByVal Text As String, ByVal FromString As String, ByVal ToString As String) As String
		Dim tmpStr As String, Loops As Long
		Loops = StrCounter(Text, FromString)
		Do Until Loops = 0
			Loops -= 1
			tmpStr = StrSplitter(Text, FromString, 2)
			tmpStr = StrSplitter(tmpStr, ToString)
			Text = Replace(Text, FromString & tmpStr & ToString, "")
		Loop
		RemoveFromTo = Text
	End Function

	Function Difference(ByVal Number1 As Object, ByVal Number2 As Object) As Object
		Difference = Number1 - Number2
		If Difference < 0 Then Difference = Difference - (Difference * 2)
	End Function

	Sub SortListbox(ByRef cList As Windows.Forms.ListBox, Optional ByVal IndexStart As Integer = 0, _
	 Optional ByVal IndexStop As Integer = -1, Optional ByVal SortAlphabetical As Boolean = True)
		Dim a As Integer, b As Integer, vsort As Object()
		If IndexStop = -1 Then IndexStop = cList.Items.Count - 1
		ReDim vsort(cList.Items.Count - 1)
		For a = 0 To cList.Items.Count - 1
			vsort(a) = cList.Items(a)
		Next
		'QuickSort(vsort)
		CleanUpArray(vsort)
		cList.Items.Clear()
		For b = 0 To UBound(vsort)
			cList.Items.Add(vsort(b))
		Next
	End Sub

	Public Sub QuickSort(ByRef vSort As Object(), Optional ByVal lngStart As Long = -32765, Optional ByVal lngEnd As Long = -32765)

		' Wird die Bereichsgrenze nicht angegeben,
		' so wird das gesamte Array sortiert

		If lngStart = -32765 Then lngStart = LBound(vSort)
		If lngEnd = -32765 Then lngEnd = UBound(vSort)

		Dim i As Long
		Dim j As Long
		Dim h
		Dim X

		i = lngStart : j = lngEnd
		X = vSort((lngStart + lngEnd) / 2)

		' Array aufteilen
		Do

			While (vSort(i) < X) : DoEvents() : i = i + 1 : End While
			While (vSort(j) > X) : DoEvents() : j = j - 1 : End While

			If (i <= j) Then
				' Wertepaare miteinander tauschen
				h = vSort(i)
				vSort(i) = vSort(j)
				vSort(j) = h
				i = i + 1 : j = j - 1
			End If
		Loop Until (i > j)

		' Rekursion (Funktion ruft sich selbst auf)
		If (lngStart < j) Then QuickSort(vSort, lngStart, j)
		If (i < lngEnd) Then QuickSort(vSort, i, lngEnd)
	End Sub

	Sub CleanUpArray(ByRef vSort As Object())
		Dim aa As Long, i As Long, colData As New Collection, b As Object
		aa = UBound(vSort)
		For i = 0 To aa
			If IsInCollection(colData, vSort(i), True) = False Then colData.Add(vSort(i))
		Next
		Erase vSort
		ReDim vSort(colData.Count - 1)
		i = -1
		For Each b In colData
			i += 1
			vSort(i) = colData(i + 1)
		Next
    End Sub

    Sub AddIfNew(ByVal List As Object, ByVal NewEntry As Object)
        If Not (List.contains(NewEntry)) Then List.add(NewEntry)
	End Sub
End Module